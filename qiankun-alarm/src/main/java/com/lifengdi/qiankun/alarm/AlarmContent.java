package com.lifengdi.qiankun.alarm;


import java.util.concurrent.atomic.AtomicLong;

/**
 * 报警内容
 * @author: Li Fengdi
 * @date: 2020/3/13 17:32
 */
public class AlarmContent {

    /**
     * 报警信息接收人
     */
    private String receiver;
    /**
     * 报警信息主题
     */
    private String subject;
    /**
     * 报警信息内容
     */
    private String content;
    /**
     * 异常出现次数
     */
    private AtomicLong count;

    /**
     * 重置次数
     */
    public void resetCount() {
        this.count.set(0);
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public AtomicLong getCount() {
        return count;
    }

    public void setCount(AtomicLong count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "AlarmContent{" +
                "receiver='" + receiver + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", count=" + count +
                '}';
    }
}