package com.lifengdi.qiankun.message.enums;

/**
 * 消息类型
 * @author: 李锋镝
 * @date: 2020-08-22 10:58
 */
public enum DingTalkMessageTypeEnum {

    TEXT("text", "文本")
    , LINK("link", "链接")
    , MARKDOWN("markdown", "markdown")
    , ACTION_CARD("actionCard", "ActionCard类型")
    , FEED_CARD("feedCard", "FeedCard类型")
    ;

    private String msgType;

    private String describe;

    DingTalkMessageTypeEnum(String msgType, String describe) {
        this.msgType = msgType;
        this.describe = describe;
    }

    public String getMsgType() {
        return msgType;
    }

    public String getDescribe() {
        return describe;
    }

    /**
     * 根据类型获取对应的枚举
     * @param msgType 消息类型
     * @return DingTalkMessageTypeEnum
     */
    public static DingTalkMessageTypeEnum getByType(String msgType) {
        for (DingTalkMessageTypeEnum messageTypeEnum : values()) {
            if (messageTypeEnum.msgType.equals(msgType)) {
                return messageTypeEnum;
            }
        }
        return null;
    }

    /**
     * 校验消息类型是否正确
     * @param msgType 消息类型
     * @return boolean
     */
    public static boolean check(String msgType) {
        for (DingTalkMessageTypeEnum messageTypeEnum : values()) {
            if (messageTypeEnum.msgType.equalsIgnoreCase(msgType)) {
                return true;
            }
        }
        return false;
    }
}
