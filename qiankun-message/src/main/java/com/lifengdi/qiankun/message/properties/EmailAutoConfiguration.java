package com.lifengdi.qiankun.message.properties;

import com.lifengdi.qiankun.message.EmailHandler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: 李锋镝
 * @date: 2020-11-26 15:36
 */
@Configuration
@EnableConfigurationProperties(EmailProperties.class)
public class EmailAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(name = "emailHandler")
    public EmailHandler emailHandler() {
        return new EmailHandler();
    }
}
