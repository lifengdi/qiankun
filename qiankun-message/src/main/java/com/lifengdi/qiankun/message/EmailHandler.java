package com.lifengdi.qiankun.message;

import com.lifengdi.qiankun.message.properties.EmailProperties;
import com.lifengdi.qiankun.message.request.EmailRequest;
import com.lifengdi.qiankun.message.request.IRequest;
import com.sun.mail.util.MailSSLSocketFactory;
import com.taobao.api.internal.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.Resource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.*;

/**
 * 邮件发送
 *
 * @author: 李锋镝
 * @date: 2020-08-22 15:14
 */
@Component
public class EmailHandler extends AbstractMessageHandler {

    private static Logger logger = LoggerFactory.getLogger(EmailHandler.class);

    @Resource
    private EmailProperties emailProperties;

    /**
     * 初始化参数
     * @return Session
     */
    private Session initProperties() {
        Properties properties = new Properties();
        properties.setProperty("mail.transport.protocol", emailProperties.getProtocol());
        properties.setProperty("mail.smtp.host", emailProperties.getHost());
        properties.setProperty("mail.smtp.port", emailProperties.getPort());
        // 使用smtp身份验证
        properties.put("mail.smtp.auth", "true");
        // 使用SSL,企业邮箱必需 start
        // 开启安全协议
        MailSSLSocketFactory mailSSLSocketFactory = null;
        try {
            mailSSLSocketFactory = new MailSSLSocketFactory();
            mailSSLSocketFactory.setTrustAllHosts(true);
        } catch (GeneralSecurityException e) {
            logger.error(e.getMessage(), e);
        }
        properties.put("mail.smtp.enable", "true");
        assert mailSSLSocketFactory != null;
        properties.put("mail.smtp.ssl.socketFactory", mailSSLSocketFactory);
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.socketFactory.fallback", "false");
        properties.put("mail.smtp.socketFactory.port", emailProperties.getPort());
        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailProperties.getAccount(), emailProperties.getPassword());
            }
        });
        // 使用SSL,企业邮箱必需 end
        // 显示debug信息 正式环境注释掉
//        session.setDebug(true);
        return session;
    }

    @Override
    public void execute(IRequest request) throws Exception {
        if (Objects.isNull(request)) {
            return;
        }
        EmailRequest emailRequest = (EmailRequest) request;
        sendWithAttachFile(emailRequest.getSender(), emailRequest.getSubject(), emailRequest.getContent(),
                emailRequest.getReceiverList(), emailRequest.getAttachments());
    }

    /**
     * 发送邮件（不带附件）
     * @param subject 邮件主题
     * @param content 邮件内容
     * @param receiverList 接收者列表,多个接收者之间用","隔开；抄送者列表使用英文问号"?"隔开
     *                     示例：
     *                     a@kd.cn?b@kd.cn
     *                     a是收件人，b为抄送人
     */
    public void send(String subject, String content, String receiverList) {
        send(null, subject, content, receiverList);
    }

    /**
     * 发送邮件（不带附件）
     * @param sender 发件人别名
     * @param subject 邮件主题
     * @param content 邮件内容
     * @param receiverList 接收者列表,多个接收者之间用","隔开；抄送者列表使用英文问号"?"隔开
     *                     示例：
     *                     a@kd.cn?b@kd.cn
     *                     a是收件人，b为抄送人
     */
    public void send(String sender, String subject, String content, String receiverList) {
        sendWithAttach(sender, subject, content, receiverList, null);
    }

    /**
     * 发送邮件（带附件）
     * @param subject 邮件主题
     * @param content 邮件内容
     * @param receiverList 接收者列表,多个接收者之间用","隔开；抄送者列表使用英文问号"?"隔开
     *                     示例：
     *                     a@kd.cn?b@kd.cn
     *                     a是收件人，b为抄送人
     * @param fileSrc 附件地址
     */
    public void sendWithAttach(String subject, String content, String receiverList, String fileSrc) {
        sendWithAttach(null, subject, content, receiverList, fileSrc);
    }

    /**
     * 发送邮件（带附件）
     * @param sender 发件人别名
     * @param subject 邮件主题
     * @param content 邮件内容
     * @param receiverList 接收者列表,多个接收者之间用","隔开；抄送者列表使用英文问号"?"隔开
     *                     示例：
     *                     a@kd.cn?b@kd.cn
     *                     a是收件人，b为抄送人
     * @param fileSrc 附件地址
     */
    public void sendWithAttach(String sender, String subject, String content, String receiverList, String fileSrc) {
        List<File> attachments = new ArrayList<>();
        if (!StringUtils.isEmpty(fileSrc)) {
            String[] fileArray = fileSrc.split(",");
            for (String filePath : fileArray) {
                if (!StringUtils.isEmpty(filePath)) {
                    attachments.add(new File(filePath));
                }
            }
        }
        sendWithAttachFile(sender, subject, content, receiverList, attachments);
    }

    /**
     * 发送邮件（带附件）
     * @param sender 发件人别名
     * @param subject 邮件主题
     * @param content 邮件内容
     * @param receiverList 接收者列表,多个接收者之间用","隔开；抄送者列表使用英文问号"?"隔开
     *                     示例：
     *                     a@kd.cn?b@kd.cn
     *                     a是收件人，b为抄送人
     * @param attachments 附件列表
     */
    public void sendWithAttachFile(String sender, String subject, String content, String receiverList,  List<File> attachments) {
        Objects.requireNonNull(subject, "邮件主题不能为空");
        Objects.requireNonNull(content, "邮件内容不能为空");
        Objects.requireNonNull(receiverList, "邮件接收人不能为空");
        try {
            Session session = initProperties();
            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setFrom(new InternetAddress(emailProperties.getAccount(), sender));// 发件人,可以设置发件人的别名
            String to, cc = null;
            if (receiverList.contains("?")) {
                String[] receiverArray = receiverList.split("\\?");
                to = receiverArray[0];
                cc = receiverArray[1];
            } else {
                to = receiverList;
            }
            Objects.requireNonNull(to, "邮件接收人不能为空");
            // 收件人,多人接收
            mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            if (!StringUtils.isEmpty(cc)) {
                mimeMessage.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
            }
            // 主题
            mimeMessage.setSubject(subject);
            // 时间
            mimeMessage.setSentDate(new Date());
            // 容器类 附件
            MimeMultipart mimeMultipart = new MimeMultipart();
            // 可以包装文本,图片,附件
            MimeBodyPart bodyPart = new MimeBodyPart();
            // 设置内容
            bodyPart.setContent(content, "text/html; charset=UTF-8");
            mimeMultipart.addBodyPart(bodyPart);
            if (!CollectionUtils.isEmpty(attachments)) {
                // 添加附件
                attachments.forEach(attach -> {
                    MimeBodyPart attachPart = new MimeBodyPart();
                    DataSource source = new FileDataSource(attach);
                    try {
                        attachPart.setDataHandler(new DataHandler(source));
                        attachPart.setFileName(MimeUtility.encodeWord(attach.getName()));
                        mimeMultipart.addBodyPart(attachPart);
                    } catch (MessagingException | UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                });

            }
            mimeMessage.setContent(mimeMultipart);
            mimeMessage.saveChanges();
            Transport.send(mimeMessage);
        } catch (MessagingException | IOException e) {
            logger.error(e.getMessage(), e);
        }
    }
}
