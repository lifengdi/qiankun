package com.lifengdi.qiankun.message;

import com.lifengdi.qiankun.message.request.IRequest;

/**
 * 消息接口
 *
 * @author: 李锋镝
 * @date: 2020-08-20 11:01
 */
public interface IMessageHandler {

    /**
     * 发送消息
     *
     * @param request 消息实体
     */
    void execute(IRequest request) throws Exception;
}
