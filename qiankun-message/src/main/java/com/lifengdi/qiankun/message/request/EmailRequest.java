package com.lifengdi.qiankun.message.request;

import java.io.File;
import java.util.List;

/**
 * @author: 李锋镝
 * @date: 2020-08-22 15:20
 */
public class EmailRequest implements IRequest {

    /**
     * 发件人别名
     */
    private String sender;

    /**
     * 邮件主题
     */
    private String subject;

    /**
     * 内容
     */
    private String content;

    /**
     * 接收者列表,多个接收者之间用","隔开；抄送者列表使用英文问号"?"隔开
     *
     * <p>a@kd.cn?b@kd.cn : a是收件人，b为抄送人</p>
     */
    private String receiverList;

    /**
     * 附件列表
     */
    private List<File> attachments;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getReceiverList() {
        return receiverList;
    }

    public void setReceiverList(String receiverList) {
        this.receiverList = receiverList;
    }

    public List<File> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<File> attachments) {
        this.attachments = attachments;
    }
}
