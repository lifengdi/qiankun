package com.lifengdi.qiankun.message.properties;

import cn.kuaidao.message.enums.DingTalkSecurityTypeEnum;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 钉钉群机器人相关配置
 *
 * @author: 李锋镝
 * @date: 2020-08-21 11:07
 */
@ConfigurationProperties(prefix = "message.dingtalk.robot")
public class DingTalkRobotProperties {

    /**
     * 钉钉群机器人URL
     */
    private String robotUrl;

    /**
     * 消息类型，消息实体类中没有指定消息类型时使用，默认text
     * <p>可用的值有：</p>
     * <p>text:文本</p>
     * <p>link:链接</p>
     * <p>markdown:markdown</p>
     * <p>actionCard:ActionCard类型</p>
     * <p>feedCard:FeedCard类型</p>
     */
    private String messageType = "text";

    private Security security;

    public String getRobotUrl() {
        return robotUrl;
    }

    public void setRobotUrl(String robotUrl) {
        this.robotUrl = robotUrl;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Security getSecurity() {
        return security;
    }

    public void setSecurity(Security security) {
        this.security = security;
    }

    public static class Security {
        /**
         * 安全设置 see{@link DingTalkSecurityTypeEnum}
         * <p>可用的值有：</p>
         * <p>keyword：关键字</p>
         * <p>sign：签名</p>
         * <p>ip：ip段</p>
         */
        private String type;

        /**
         * 签名的密钥
         */
        private String signSecret;

        /**
         * 安全设置
         */
        private String keyword;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getSignSecret() {
            return signSecret;
        }

        public void setSignSecret(String signSecret) {
            this.signSecret = signSecret;
        }

        public String getKeyword() {
            return keyword;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }
    }
}
