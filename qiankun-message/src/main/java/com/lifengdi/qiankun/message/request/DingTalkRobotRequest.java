package com.lifengdi.qiankun.message.request;

import com.dingtalk.api.request.OapiRobotSendRequest;

/**
 * 钉钉机器人消息实体类
 *
 * @author: 李锋镝
 * @date: 2020-08-20 14:16
 */
public class DingTalkRobotRequest extends OapiRobotSendRequest implements IRequest {
}
