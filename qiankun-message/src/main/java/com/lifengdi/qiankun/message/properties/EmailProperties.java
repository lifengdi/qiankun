package com.lifengdi.qiankun.message.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: 李锋镝
 * @date: 2020-11-26 14:26
 */
@ConfigurationProperties(prefix = "message.email")
public class EmailProperties {

    // 登录账户
    private String account;
    // 登录密码
    private String password;
    // 服务器地址
    private String host;
    // 端口
    private String port;
    // 协议
    private String protocol;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
}
