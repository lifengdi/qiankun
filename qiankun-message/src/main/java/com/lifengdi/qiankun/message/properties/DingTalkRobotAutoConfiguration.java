package com.lifengdi.qiankun.message.properties;

import com.lifengdi.qiankun.message.DingTalkRobotMessageHandler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: 李锋镝
 * @date: 2020-11-26 15:34
 */
@Configuration
@EnableConfigurationProperties(DingTalkRobotProperties.class)
public class DingTalkRobotAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(name = "dingTalkRobotMessageHandler")
    public DingTalkRobotMessageHandler dingTalkRobotMessageHandler() {
        return new DingTalkRobotMessageHandler();
    }
}
