package com.lifengdi.qiankun.message;

import com.lifengdi.qiankun.message.request.IRequest;

/**
 * @author: 李锋镝
 * @date: 2020-08-20 16:16
 */
public abstract class AbstractMessageHandler implements IMessageHandler {

    @Override
    public void execute(IRequest request) throws Exception {

    }
}
