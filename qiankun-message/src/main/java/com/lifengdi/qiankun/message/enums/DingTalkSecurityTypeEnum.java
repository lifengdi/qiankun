package cn.kuaidao.message.enums;

/**
 * 安全设置类型枚举
 *
 * @author: 李锋镝
 * @date: 2020-08-22 10:47
 */
public enum DingTalkSecurityTypeEnum {

    NULL("", null)
    , KEYWORD("keyword", "关键字")
    , SIGN("sign", "签名")
    , IP("ip", "ip段")
    ;

    private String securityType;

    private String describe;

    DingTalkSecurityTypeEnum(String securityType, String describe) {
        this.securityType = securityType;
        this.describe = describe;
    }

    public String getSecurityType() {
        return securityType;
    }

    public String getDescribe() {
        return describe;
    }

    /**
     * 根据类型获取枚举
     * @param securityType securityType
     * @return SecurityTypeEnum
     */
    public static DingTalkSecurityTypeEnum getByType(String securityType) {
        for (DingTalkSecurityTypeEnum dingTalkSecurityTypeEnum : values()) {
            if (dingTalkSecurityTypeEnum.securityType.equalsIgnoreCase(securityType)) {
                return dingTalkSecurityTypeEnum;
            }
        }
        return NULL;
    }
}
