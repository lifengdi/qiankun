# qiankun

## 介绍
乾坤

## 软件架构
提供一些项目通用的工具类以及一些开源项目的二次封装。

## 使用说明

1.  qiankun-common：基础类
2.  qiankun-alarm：报警相关
3.  qiankun-lucene：对lucene的封装
4.  qiankun-message：发送消息

## 联系我
个人博客：[https://www.lifengdi.com](https://www.lifengdi.com)