@echo off

cd ../../
call mvn versions:set -DnewVersion=1.0.1-RELEASE
echo "mvn versions set success"

call mvn clean install -Dmaven.test.skip=true deploy
echo "mvn deploy success"

cd shell/windows/
:exit