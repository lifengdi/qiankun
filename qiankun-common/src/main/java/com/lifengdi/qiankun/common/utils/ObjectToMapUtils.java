package com.lifengdi.qiankun.common.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @author: 李锋镝
 * @date: 2020-06-30 15:40
 */
public class ObjectToMapUtils {

    private static ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }
    /**
     * bean转map
     * @param bean 实体
     * @param <T> 泛型
     * @return map
     */
    public static <T> Map<String, Object> objectToMap(T bean) {
        Map<String, Object> map = new HashMap<>();
        if (bean != null) {
            BeanMap beanMap = BeanMap.create(bean);
            for (Object key : beanMap.keySet()) {
                map.put(key.toString(), beanMap.get(key));
            }
        }
        return map;
    }

    public static <T> T mapEntity(String source, Class<T> clazz) {
        if (StringUtils.isEmpty(source)) {
            return null;
        }
        return mapToObject(source, clazz);
    }

    /**
     * 将map转换为bean
     *
     * @param map map
     * @param clazz clazz
     * @return 转换后的
     */
    public static <T> T mapToObject(Map map, Class<T> clazz) {
        return mapEntity(mapToSource(map), clazz);
    }

    public static String mapToSource(Map map) {
        try {
            return objectMapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param source source
     * @param clazz clazz
     * @return 转换后的
     */
    public static <T> T mapToObject(String source, Class<T> clazz) {
        try {
            return objectMapper.readValue(source, clazz);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 对象转MultiValueMap
     * @param bean 实体
     * @param <T> T
     * @return MultiValueMap
     */
    public static <T> MultiValueMap<String, String> objectToMultiValueMap(T bean) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        if (bean != null) {
            BeanMap beanMap = BeanMap.create(bean);
            for (Object key : beanMap.keySet()) {
                List<String> list = map.getOrDefault(key, new ArrayList<>());
                Object value = beanMap.get(key);
                if (Objects.nonNull(value)) {
                    list.add(value.toString());
                }
                if (!CollectionUtils.isEmpty(list)) {
                    map.put(key.toString(), list);
                }
            }
        }
        return map;
    }
}
