package com.lifengdi.qiankun.common.response;


import java.util.List;


/**
 * 报文响应类 分页
 * @author: 李锋镝
 * @date: 2020-06-30 11:17
 */
public class Page<T> {

    /**
     * 当前页
     */
    private long pageIndex;
    /**
     * 每页显示数量
     */
    private long countPerPage;
    /**
     * 总页数
     */
    private long pageCount;
    /**
     * 总数据量
     */
    private long totalCount;

    /**
     * 数据
     */
    private List<T> data;

    public Page() {
    }

    public Page(long pageIndex, long countPerPage, long totalCount, List<T> data) {
        this.pageIndex = pageIndex;
        this.countPerPage = countPerPage;
        this.totalCount = totalCount;
        this.data = data;
        this.pageCount = this.calcPageCount(totalCount, countPerPage);
    }

    public long getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(long pageIndex) {
        this.pageIndex = pageIndex;
    }

    public long getCountPerPage() {
        return countPerPage;
    }

    public void setCountPerPage(long countPerPage) {
        this.countPerPage = countPerPage;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    private long calcPageCount(long totalCount, long countPerPage) {
        return totalCount % countPerPage == 0 ? totalCount / countPerPage : totalCount / countPerPage + 1;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageIndex=" + pageIndex +
                ", countPerPage=" + countPerPage +
                ", pageCount=" + pageCount +
                ", totalCount=" + totalCount +
                ", data=" + data +
                '}';
    }
}
