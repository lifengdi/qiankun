package com.lifengdi.qiankun.common.global;

import com.lifengdi.qiankun.common.utils.EnvironmentUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 服务环境信息
 */
@Configuration
public class EnvironmentInfo {

    /**
     * 服务名
     */
    @Value("${spring.application.name:ApplicationName}")
    public String applicationName;

    /**
     * 环境
     */
    @Value("${info.env:dev}")
    public String env;

    /**
     * 初始化静态参数
     * @return 0 无实意
     */
    @Bean
    public int initStaticEnvironmentInfo() {
        EnvironmentUtils.setApplicationName(applicationName);
        EnvironmentUtils.setEnv(env);
        return 0;
    }

}
