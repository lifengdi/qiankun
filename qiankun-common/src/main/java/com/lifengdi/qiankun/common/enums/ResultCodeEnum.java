package com.lifengdi.qiankun.common.enums;

/**
 * 响应码枚举
 */
public enum ResultCodeEnum {

    SUCCESS_0("0", "SUCCESS"),
    SUCCESS("200", "SUCCESS"),
    BAD_REQUEST("400", "BAD_REQUEST"),
    NOT_LOGIN("401", "帐号未登陆"),
    LOCK("403", "账号被锁定"),
    FAIL("500", "FAIL"),
    ;

    private String code;

    private String msg;

    ResultCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
