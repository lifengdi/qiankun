package com.lifengdi.qiankun.common.enums;

import com.aliyun.oss.model.GroupGrantee;
import com.aliyun.oss.model.Permission;

/**
 * @author: 李锋镝
 * @date: 2020-08-05 15:16
 */
public enum AccessControlListEnum {

    /**
     * This is only for object, means the permission inherits the bucket's
     * permission.
     */
    Default("default"),

    /**
     * The owner has the {@link Permission#FullControl}, other
     * {@link GroupGrantee#AllUsers} does not have access.
     */
    Private("private"),

    /**
     * The owner has the {@link Permission#FullControl}, other
     * {@link GroupGrantee#AllUsers} have read-only access.
     */
    PublicRead("public-read"),

    /**
     * Both the owner and {@link GroupGrantee#AllUsers} have
     * {@link Permission#FullControl}. It's not safe and thus not recommended.
     */
    PublicReadWrite("public-read-write");

    private String cannedAclString;

    private AccessControlListEnum(String cannedAclString) {
        this.cannedAclString = cannedAclString;
    }

    @Override
    public String toString() {
        return this.cannedAclString;
    }

    public static AccessControlListEnum parse(String acl) {
        for (AccessControlListEnum cacl : AccessControlListEnum.values()) {
            if (cacl.toString().equals(acl)) {
                return cacl;
            }
        }

        throw new IllegalArgumentException("Unable to parse the provided acl " + acl);
    }
}
