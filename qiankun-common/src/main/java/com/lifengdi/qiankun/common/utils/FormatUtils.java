package com.lifengdi.qiankun.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 格式化工具类
 * @author: Li Fengdi
 * @date: 2020/3/13 17:42
 */
public interface FormatUtils {

    String PREFIX = "[";

    String SUFFIX = "]";

    String CONNECTOR = ",";

    /**
     * 将字符串用中括号括起来
     * @param s 字符串
     * @return [s]
     */
    static String wrapStringWithBracket(String s) {
        return PREFIX + s + SUFFIX;
    }

    /**
     * 将字符串用中括号括起来并追加在原始字符串后边
     * <p>如果要追加的字符串两边已经有了中括号则不会再次追加中括号</p>
     * @param original 原始字符串
     * @param append 需要追加到original的字符串
     * @return original,[append]
     */
    static String appendStringWithBracket(String original, String append) {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(original)) {
            sb.append(original);
            sb.append(CONNECTOR);
        }
        if (!append.startsWith(PREFIX)) {
            sb.append(PREFIX);
        }
        sb.append(append);
        if (!append.endsWith(SUFFIX)) {
            sb.append(SUFFIX);
        }
        return sb.toString();
    }

    /**
     * 分隔字符串，取出被中括号包裹的数据
     * @param str 被中括号包裹、使用英文逗号连接的字符串
     * @return 解析之后的数据
     */
    static List<Object> splitStringWithBracket(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        String[] split = str.split(CONNECTOR);
        return Arrays.asList(split).stream().map(s -> {
            s = s.replace(PREFIX, "");
            s = s.replace(SUFFIX, "");
            return s;
        }).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        String str = "[1],[2],[3]";
        System.out.println(appendStringWithBracket(str, "4]"));
        System.out.println(splitStringWithBracket(str));
    }

}
