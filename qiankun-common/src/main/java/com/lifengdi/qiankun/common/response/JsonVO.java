package com.lifengdi.qiankun.common.response;

import com.lifengdi.qiankun.common.enums.ResultCodeEnum;
import com.lifengdi.qiankun.common.exception.ApiException;

import java.io.Serializable;

/**
 * 报文响应类
 */
public class JsonVO<T> implements Serializable {

    private static final long serialVersionUID = 7311365896121051697L;

    private String code;

    private String msg;

    private T data;

    public JsonVO() {
    }

    public JsonVO(ApiException e) {
        this.code = e.getCode();
        this.msg = e.getMessage();
    }

    public JsonVO(ResultCodeEnum resultCodeEnum, T data) {
        this.code = resultCodeEnum.getCode();
        this.msg = resultCodeEnum.getMsg();
        this.data = data;
    }

    public JsonVO(ResultCodeEnum resultCodeEnum) {
        this.code = resultCodeEnum.getCode();
        this.msg = resultCodeEnum.getMsg();
    }

    public JsonVO(T data) {
        this(ResultCodeEnum.SUCCESS, data);
    }

    public JsonVO(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T> JsonVO<T> fail(String msg, T data) {
        return new JsonVO<>(ResultCodeEnum.FAIL, data);
    }

    public static <T> JsonVO<T> fail(String msg) {
        return new JsonVO<>(ResultCodeEnum.FAIL.getCode(), msg, null);
    }

    public static <T> JsonVO<T> fail(Throwable e) {
        if (e instanceof ApiException) {
            return new JsonVO<>((ApiException) e);
        }
        return new <T> JsonVO<T>(ResultCodeEnum.FAIL, null);
    }

    public static <T> JsonVO<T> fail() {
        return new JsonVO<>(ResultCodeEnum.FAIL);
    }


    public static <T> JsonVO<T> fail(String code, String msg) {
        if (ResultCodeEnum.SUCCESS.getCode().equals(code)) {
            throw new RuntimeException("Status code is invalid");
        }
        return new JsonVO<>(code, msg, null);
    }

    public static <T> JsonVO<T> success(T data) {
        return new JsonVO<>(ResultCodeEnum.SUCCESS, data);
    }

    public static <T> JsonVO<T> success(String msg, T data) {
        return new <T> JsonVO<T>(ResultCodeEnum.SUCCESS.getCode(), msg, data);
    }

    public static <T> JsonVO<T> badRequestError(String message) {
        return new JsonVO<>(ResultCodeEnum.BAD_REQUEST.getCode(), message, null);
    }

    public static <T> JsonVO<T> success() {
        return new JsonVO<>(ResultCodeEnum.SUCCESS);
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "JsonVO{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

}
