package com.lifengdi.qiankun.common.exception;

/**
 * ApiException
 */
public class ApiException extends RuntimeException {

    private static final long serialVersionUID = -4421215804404094704L;

    private String code;

    private String msg;

    public ApiException(String message) {
        super(message);
        this.msg = message;
    }

    public ApiException(String code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ApiException{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }

}
